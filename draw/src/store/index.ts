/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */
 
import { defineStore } from 'pinia';

/* eslint-disable */ 
export const useNFTStore = defineStore('nft', {
  state: () =>
    ({
      nftUpdateTime: 0,
    }),
  actions: {
    setUpdateTime(timestamp: number) {
      this.$patch(state => {
        state.nftUpdateTime = timestamp;
      });
    },
  },
});
