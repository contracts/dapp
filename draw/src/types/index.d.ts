/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */

export {};

declare global {
  interface Window {
    chainMaker: {
      createUserContract:(param:{
        body:{
          contractName:string;
          contractVersion?:string;
          contractFile?:string;
          runtimeType?: string,
          params?: any,
        },
        ticket:string;
      })=>void;

      invokeUserContract:(param:{
        body:{
          contractName:string;
          method:string;
          runtimeType?: string,
          params?: any,
        },
        ticket:string;
      })=>void;
    };
  }
}
declare module 'crypto-js';
declare module 'js-cookie';
declare module 'vue-buffer';
