/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */

import axios from 'axios';
// import store from '@/store'

// 创建实例
const instance = axios.create({
  baseURL: '/backend',
  // baseURL:'/api',
  timeout: 10000,
});
// 创建含有用户信息的请求实例
// const instances = axios.create({
//   baseURL: process.env.VUE_APP_API_HOST,
//   // baseURL:'/api',
//   timeout: 10000,
// });

// // 请求拦截
// instances.interceptors.request.use((config:AxiosRequestConfig) => {
//   const authinfo = getAuthInfo();
//   if (!authinfo || !authinfo.token) {
//     tologin();
//     throw (new Error('先请登录！'));
//   }
//   if (config?.headers) {
//     // eslint-disable-next-line no-param-reassign
//     config.headers['x-token'] = authinfo.token;
//   }
//   return config;
// }, (error) => Promise.reject(error));

// 添加响应拦截器
instance.interceptors.response.use(({
  data,
}) => data, (error) => Promise.reject(error));

export const post = (url:string) => (data:unknown):Promise<any> => instance.post(url, data);
export const get = (url:string) => (params:unknown):Promise<any> => instance.get(url, {
  params,
});
// export const posts = (url:string) => (data:unknown) => instances.post(url, data);
// export const gets = (url:string) => (params:unknown) => instances.get(url, {
//   params,
// });

export const upload = (url:string) => (data:unknown):Promise<any> => instance.post(url, data, {
  headers: { 'Content-Type': 'multipart/form-data' },
});
