/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */

import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
// import HomeView from '../views/HomeView.vue';

const routes: Array<RouteRecordRaw> = [
  {
    path: '',
    redirect: '/draw/create/artboard',
  },
  {
    path: '/draw',
    name: 'draw',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "witness" */ '../views/Draw/index.vue'),
    children: [{
      path: 'create',
      name: 'create',
      component: () => import(/* webpackChunkName: "witness" */ '../views/Draw/createSpace/index.vue'),
      children: [{
        path: 'artboard',
        name: 'artboard',
        component: () => import(/* webpackChunkName: "witness" */ '../views/Draw/createSpace/subpage/artboard.vue'),
      }, {
        path: 'upload',
        name: 'uploadpic',
        component: () => import(/* webpackChunkName: "witness" */ '../views/Draw/createSpace/subpage/uploadpic.vue'),
      }, {
        path: '/draw/create/:pathMatch(.*)*',
        redirect: '/draw/create/artboard',
      }],
    },
    {
      path: 'picture',
      name: 'picture',
      component: () => import(/* webpackChunkName: "witness" */ '../views/Draw/pictureSpace/index.vue'),
      children: [{
        path: 'transfer',
        name: 'PictureTransfer',
        component: () => import(/* webpackChunkName: "witness" */ '../views/Draw/pictureSpace/subpage/transferpic.vue'),
      }, {
        path: 'query',
        name: 'PictureQuery',
        component: () => import(/* webpackChunkName: "witness" */ '../views/Draw/pictureSpace/subpage/picquery.vue'),
      }, {
        path: '/draw/picture/:pathMatch(.*)*',
        redirect: '/draw/picture/transfer',
      }],
    },
    {
      path: '/draw/:pathMatch(.*)*',
      redirect: '/draw/create/artboard',
    }],
  },
  {
    path: '/:pathMatch(.*)*',
    // 方法接收目标路由作为参数
    // return 重定向的字符串路径/路径对象
    // console.log(to);
    redirect: () => ({ name: 'artboard' })
    ,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
