/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */

import moment from 'moment';
import type { App } from 'vue';

export default {
  install: (app: App) => {
    // eslint-disable-next-line no-param-reassign
    app.config.globalProperties.$filters = {
      unixDateFormat(value: string | number) {
        return moment.unix(Number(value)).format('YYYY-MM-DD HH:mm:ss');
      },
      dateFormat(value: string | number) {
        return moment(Number(value)).format('YYYY-MM-DD HH:mm:ss');
      },
    };
  },
};
