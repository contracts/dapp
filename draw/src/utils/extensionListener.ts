/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */

import { decode } from 'js-base64';
import { ElMessage } from 'element-plus';
import { useNFTStore } from '@/store/index';

export default function extensionListener() {
  window.addEventListener('message', (event) => {
    if (event.source !== window) {
      return;
    }
    const NFTStore = useNFTStore();
    const storage = localStorage.getItem('apps_notification_event');
    const eventlist = storage ? JSON.parse(storage) : {};
    const { data, ticket } = event.data;
    const body = eventlist[ticket];
    if (data?.chainNodeResult?.contractResult?.result) {
      console.log(data);
      if (data.status === 'error' || data.status === 'done') {
        const info = decode(data.chainNodeResult.contractResult.result);
        if (body?.params?.method === 'listNFT') {
          if (data.status === 'error' && data.detail === 'Fail') {
            /* eslint-disable */
            ElMessage.error('获取所有画作NFT失败' + info);
          } else if (data.status === 'done') {
            const infoJson = JSON.parse(info);
            console.log('infojson', infoJson);
            localStorage.setItem('nft_info_from_plugin', JSON.stringify(infoJson));
            NFTStore.setUpdateTime(ticket)
          }
        } else if (body?.params?.method === 'nftTransferFrom') {
          if (data.status === 'error' && data.detail === 'Fail') {
            /* eslint-disable */
            ElMessage.error('画作NFT转增失败, ' + info);
          } else if (data.status === 'done') {
            ElMessage.success('画作NFT转增成功,点击“从链上获取最新数据”按钮刷新画作数据');
          }
        } else if (body?.params?.method === 'mintForMyself') {
          if (data.status === 'error' && data.detail === 'Fail') {
            /* eslint-disable */
            ElMessage.error('画作上链NFT制作失败' + info);
          } else if (data.status === 'done') {
            ElMessage.success('画作上链NFT制作成功,快去“画作空间”查看吧～');
          }
        } else if (body?.params?.method === 'ownerOf') {
          if (data.status === 'error' && data.detail === 'Fail') {
            ElMessage.error('通过tokenId查询归属账户地址失败' + info);
            /* eslint-disable */
          } else if (data.status === 'done') {
            localStorage.setItem('tokenOwner', info);
            NFTStore.setUpdateTime(ticket)
          }
        }
      }
    } else if (body?.params?.method === 'ownerOf' && data?.status ==='done') {
        localStorage.setItem('tokenOwner', '');
        NFTStore.setUpdateTime(ticket)
      }
  }, false);
}
