/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */

import cookie from 'js-cookie';
// 更新用户token信息
export const updateAuthInfo = (authinfo : any) => {
  if (!authinfo.token || !authinfo.username) {
    return;
  }
  try {
    cookie.set('cm_authinfo', JSON.stringify(authinfo), {
      expires: 7, // 7 day
    });
  } catch (e) {
    sessionStorage.setItem('cm_authinfo', JSON.stringify(authinfo));
  }
};
// 跳转到登录页
// export const tologin = () => {
//   // debugger
//   window.location.href = `${process.env.VUE_APP_LOGIN_PHAT}?response_type=code&client_id=app&state=UqyzX1EU4QRECtITWI7QdfjbBUjh9cfh&redirect_uri=https://portalbackend.chainmaker.org.cn/base/loginWithChainmakerAuthenticationCenter?loginUrl=${window.location.href}`;
//   // window.location.href = `${process.env.VUE_APP_LOGIN_PHAT}?response_type=code&client_id=app&state=UqyzX1EU4QRECtITWI7QdfjbBUjh9cfh&redirect_uri=http://testnet.chainmaker.org.cn:8888/base/loginWithChainmakerAuthenticationCenter?loginUrl=${window.location.href}`;
// };
// 获取本地用户信息
export const getAuthInfo = () => {
  const ainfo = cookie.get('cm_authinfo');
  const authinfo = sessionStorage.getItem('cm_authinfo');

  return JSON.parse((ainfo || authinfo) as string);
};

export const clearAuthInfo = () => {
  cookie.remove('cm_authinfo');
  sessionStorage.removeItem('cm_authinfo');
};
