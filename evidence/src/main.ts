/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */

import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import filter from './filter';
import './index.css';
import 'element-plus/dist/index.css';
import extensionListener from './utils/extensionListener';

const app = createApp(App);
app.use(store).use(filter).use(router).mount('#app');
extensionListener();
