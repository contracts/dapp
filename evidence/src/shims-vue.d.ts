/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */

/* eslint-disable */
declare module '*.vue' {
  import type { DefineComponent } from 'vue'
  const component: DefineComponent<{}, {}, any>
  export default component
}

declare module 'vue/types/vue' {
  import type { RouteLocationNormalizedLoaded,Router } from 'vue-router';
  interface Vue {
    $filters: any;
    $route: RouteLocationNormalizedLoaded;
    $router: Router;
  }
}
