/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */

import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
// import HomeView from '../views/HomeView.vue';

const routes: Array<RouteRecordRaw> = [
  {
    path: '',
    redirect: '/witness/upload/text',
  },
  {
    path: '/witness',
    name: 'witness',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "witness" */ '../views/Witness/index.vue'),
    children: [{
      path: 'upload',
      name: 'upload',
      component: () => import(/* webpackChunkName: "witness" */ '../views/Witness/upload/index.vue'),
      children: [{
        path: 'text',
        name: 'uploadtext',
        component: () => import(/* webpackChunkName: "witness" */ '../views/Witness/upload/subpage/uploadtext.vue'),
      }, {
        path: 'pic',
        name: 'uploadpic',
        component: () => import(/* webpackChunkName: "witness" */ '../views/Witness/upload/subpage/uploadpic.vue'),
      }, {
        path: 'file',
        name: 'uploadfile',
        component: () => import(/* webpackChunkName: "witness" */ '../views/Witness/upload/subpage/uploadfile.vue'),
      }, {
        path: '/witness/upload/:pathMatch(.*)*',
        redirect: '/witness/upload/text',
      }],
    },
    {
      path: 'uploadsuccess',
      name: 'uploadsuccess',
      component: () => import(/* webpackChunkName: "witness" */ '../views/Witness/upload/success.vue'),
    },
    {
      path: 'uploadfail',
      name: 'uploadfail',
      component: () => import(/* webpackChunkName: "witness" */ '../views/Witness/upload/fail.vue'),
    },
    {
      path: 'check',
      name: 'check',
      component: () => import(/* webpackChunkName: "witness" */ '../views/Witness/check/index.vue'),
      children: [{
        path: 'text',
        name: 'checktext',
        component: () => import(/* webpackChunkName: "witness" */ '../views/Witness/check/subpage/checktext.vue'),
      }, {
        path: 'file',
        name: 'checkfile',
        component: () => import(/* webpackChunkName: "witness" */ '../views/Witness/check/subpage/checkfile.vue'),
      }, {
        path: '/witness/check/:pathMatch(.*)*',
        redirect: '/witness/check/text',
      }],
    },
    {
      path: 'checksuccess',
      name: 'checksuccess',
      component: () => import(/* webpackChunkName: "witness" */ '../views/Witness/check/success.vue'),
    },
    {
      path: 'checkfail',
      name: 'checkfail',
      component: () => import(/* webpackChunkName: "witness" */ '../views/Witness/check/fail.vue'),
    },
    {
      path: '/witness/:pathMatch(.*)*',
      redirect: '/witness/upload/text',
    }],
  },
  // {
  //   path: '/vote',
  //   name: 'vote',
  //   component: () => import(/* webpackChunkName: "vote" */ '../views/Vote/index.vue'),
  //   children: [{
  //     path: 'list',
  //     name: 'votelist',
  //     component: () => import(/* webpackChunkName: "vote" */ '../views/Vote/pages/list.vue'),
  //   },
  //   {
  //     path: 'create',
  //     name: 'votecreate',
  //     component: () => import(/* webpackChunkName: "vote" */ '../views/Vote/pages/create.vue'),
  //   },
  //   {
  //     path: 'detail/:projectId',
  //     name: 'votedetail',
  //     component: () => import(/* webpackChunkName: "vote" */ '../views/Vote/pages/detail.vue'),
  //   },
  //   {
  //     path: '/vote/:pathMatch(.*)*',
  //     redirect: '/vote/list',
  //   }],
  // },
  {
    path: '/:pathMatch(.*)*',
    // 方法接收目标路由作为参数
    // return 重定向的字符串路径/路径对象
    // console.log(to);
    redirect: () => ({ name: 'uploadtext' })
    ,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
