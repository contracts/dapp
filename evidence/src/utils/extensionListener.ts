/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */

import router from '@/router';
import { decode } from 'js-base64';

export default function extensionListener() {
  window.addEventListener('message', (event) => {
    if (event.source !== window) {
      return;
    }
    const storage = localStorage.getItem('apps_notification_event');
    const eventlist = storage ? JSON.parse(storage) : {};
    // console.log(event);
    const { data, ticket } = event.data;
    if (data?.chainNodeResult?.contractResult?.result) {
      console.log('Page script received:');
      console.log(data);
      if (data.status === 'error' || data.status === 'done') {
        const body = eventlist[ticket];
        const info = decode(data.chainNodeResult.contractResult.result);
        // info.time
        if (body?.params?.method === 'save') {
          if (data.status === 'error' && data.detail === 'Fail') {
            router.push('/witness/uploadfail');
          } else if (data.status === 'done') {
            const infojson = JSON.parse(info);
            infojson.timestamp = data.timestamp;
            localStorage.setItem('upload_from_plugin_info', JSON.stringify(infojson));
            router.push('/witness/uploadsuccess');
          }
        } else if (body?.params?.method === 'findByHash') {
          if (data.status === 'error' && data.detail === 'Fail') {
            router.push('/witness/checkfail');
          } else if (data.status === 'done') {
            localStorage.setItem('check_from_plugin_info', info);
            router.push('/witness/checksuccess');
          }
        } else if (body?.params?.method === 'issueProject') {
          if (data.status === 'done') {
            router.push('/vote/list');
          }
        }
      }
    }
  }, false);
}
