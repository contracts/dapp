/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */

import { SHA256, lib } from 'crypto-js';

import { UploadRawFile } from 'element-plus';

export const createHashByFile = (file:UploadRawFile):Promise<string> => new Promise((resolve, reject) => {
  const reader = new FileReader();
  reader.readAsArrayBuffer(file);
  reader.onload = () => {
    const wordArray = lib.WordArray.create(reader.result as unknown as number[]);
    console.log(reader.result);
    const hash:string = SHA256(wordArray).toString();
    resolve(hash);
  };
  reader.onerror = reject;
});
