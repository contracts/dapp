/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */

// import JSZip from 'jszip';
// import { saveAs } from 'file-saver';
// export function downloadCerByText(text,fileName){
//   var blob = new Blob([text],{
//     type:"application/x-x509-ca-cert"
//   });
//   var reader = new FileReader();
//   reader.readAsDataURL(blob);
//   reader.onload = function(e){
//     let a = document.createElement('a');
//     a.download = fileName;
//     a.href = e.target.result;
//     document.body.appendChild(a);
//     a.click();
//     document.body.removeChild(a);
//   }
// }
// const decodeBase64 = (str) => decodeURIComponent(escape(atob(str)));
// export function downloadZipByTexts(list,pkgName){
//   const zip = new JSZip();
//   try {
//     list.forEach(({ data, name, fileType, dataType = 'base64' }) => {
//       const blobPart = dataType === 'base64' ? decodeBase64(data) : data;
//       const option = fileType ? { type: fileType } : {};
//       const blob = new Blob([blobPart], option);
//       zip.file(name, blob);
//     });
//     zip.generateAsync({ type: 'blob' }).then((content) => {
//       // depend file-saver
//       saveAs(content, pkgName);
//     });
//   } catch (e) {
//     console.error(e);
//   }
// }

const types = {
  'image/jpg': 'jpg',
  'image/jpeg': 'jpeg',
  'image/png': 'png',
};
type Types = typeof types;

export function dataURLtoBlob(dataurl:string) {
  const arr = dataurl.split(',');
  const mime = arr?.[0]?.match(/:(.*?);/)?.[1];
  const bstr = atob(arr[1]);
  let n = bstr.length;
  const u8arr = new Uint8Array(n);
  while (n > 0) {
    n -= 1;
    u8arr[n] = bstr.charCodeAt(n);
  }
  return new Blob([u8arr], { type: mime });
}

export function pictureImageCompressor(image:HTMLImageElement, maxsize:number, filetype:keyof Types) {
  const msize = maxsize * 5;
  const type:Types[keyof Types] = filetype ? types[filetype] : 'jpeg';
  const { width, height } = image;
  let w = width;
  let h = height;
  if (width * height > msize) {
    const scale = Math.sqrt((width * height) / msize);
    w /= scale;
    h /= scale;
  }
  const canvas = document.createElement('canvas');
  canvas.width = w;
  canvas.height = h;
  const ctx = canvas.getContext('2d');
  ctx?.drawImage(image, 0, 0, w, h);
  const data = canvas.toDataURL(filetype, 0.8);
  const blob = dataURLtoBlob(data);
  return new File([blob], `${new Date().getTime()}.${type}` || 'jpg');
  // document.body.append(canvas);
}

export const pictureFileCompressor = (file:File, maxsize:number):Promise<File> => new Promise((resolve, reject) => {
  const image = new Image();
  // document.body.append(image);
  image.onload = () => {
    resolve(pictureImageCompressor(image, maxsize, file.type as keyof Types));
  };
  image.onerror = reject;
  image.src = URL.createObjectURL(file);
});
