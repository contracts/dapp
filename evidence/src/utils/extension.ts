/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */

interface ClassMap{
  success: string;
  info: string;
  error: string;
  warn: string;
  done: string;
}

export interface PluginResult {
  hash: string;
  txId: string;
  timestamp?:number;
}

type ClassKey = keyof ClassMap;
export const extensionInstallCheck = (callback:(arg:boolean)=>void) => {
  callback(!!window.chainMaker);
};

const classMap:ClassMap = {
  success: 'notification-succ',
  info: 'notification-run',
  error: 'notification-error',
  warn: 'notification-warn',
  done: 'notification-run',
};
export const getNotificationClass = (status:ClassKey):string => classMap[status];

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const setNotificationEvent = (eventBody: any, ticket:string) => {
  const event = localStorage.getItem('apps_notification_event');
  const eventlist = event ? JSON.parse(event) : {};
  eventlist[ticket] = { overtime: new Date().getTime() + 60 * 1000, ...eventBody };
  localStorage.setItem('apps_notification_event', JSON.stringify(eventlist));
};

export const invokeUserContract = (params: any, contractName:string) => {
  const ticket = Date.now().toString();
  window.chainMaker.invokeUserContract({
    body: {
      contractName,
      method: params.method,
      params,
    },
    ticket,
  });
  setNotificationEvent({
    operation: 'invokeUserContract',
    contractName,
    method: params.method,
    eventId: ticket,
    params,
  }, ticket);
};

export const removeOverTimeEvent = (ticket?:string) => {
  const event = localStorage.getItem('apps_notification_event');
  const eventlist = event ? JSON.parse(event) : {};
  if (ticket) {
    delete eventlist[ticket];
  }
  const keys = Object.keys(eventlist);
  keys.forEach((name) => {
    if (eventlist[name].overtime < new Date().getTime()) {
      delete eventlist[name];
    }
  });
  localStorage.setItem('apps_notification_event', JSON.stringify(eventlist));
};

export const getUploadPluginInfo = () => JSON.parse(localStorage.getItem('upload_from_plugin_info') || '') as PluginResult;
export const getCheckPluginInfo = () => JSON.parse(localStorage.getItem('check_from_plugin_info') || '') as PluginResult;
