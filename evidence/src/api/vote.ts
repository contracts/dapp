/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */

import { post } from './request';
import { CommonResponse, Fetch, PageParam } from './type';

export interface VoteItem {
  createAt:string;
  deleteAt?:string;
  'end_time':string;
  id: number;
  picUrl:string;
  projectId: string;
  rule:string;
  scenery:string;
  'start_time':string;
  state:string;
  title:string;
  updateAt:string;
}
export interface VoteListResponse extends CommonResponse{
  voteList:VoteItem[];
  totalCount: number;
}
export interface VoteDetail{
  'end_time':string;
  'rule': string;
  'scenery':string;
  'start_time': string;
  'title':string;
  'totalCount': number;
  'voteDetailList':{
    'id': number;
    'createAt': string;
    'updateAt': string;
    'deleteAt': null,
    'itemId': string;
    'picItemUrl': string;
    'url': string;
    'number': number;
    'sum_Number': number;
    'description': string;
  }[];
}
export interface VoteDetailResponse extends CommonResponse, VoteDetail{

}
interface VoteDetailParam{
  ProjectId: string;
}

export const getVoteList: Fetch<PageParam, VoteListResponse> = post('/get_voteList');
export const getVoteDetail: Fetch<VoteDetailParam, VoteDetailResponse> = post('/get_voteDetail');
