/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */

import { upload } from './request';
import { Fetch } from './type';
// import { Fetch, ResponseData, ResponseInfo } from './type';

export const uploadFile: Fetch<unknown, {
  code:number;
  msg:string;
  url:string;
}> = upload('/upload_File');

export const createUrl = (url:string):string => (`${window.location.origin}/backend/${url}`);
